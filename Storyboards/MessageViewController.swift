//
//  MessageViewController.swift
//  Storyboards
//
//  Created by Neurox Gòmez on 9/13/17.
//  Copyright © 2017 Juan Jose Gomez Lopez. All rights reserved.
//

import Foundation
import UIKit

class MessageViewController: UIViewController {
    
    let messages = [
        "Ouch, that hurts",
        "Please don't do that again",
        "Why did you press that?",
        ]
    
    @IBOutlet weak var message: UILabel!
    
    @IBAction func changeMessage() {
        message.text = messages[
            Int(arc4random_uniform(
            UInt32(messages.count)))
        ]
    }
    
    @IBAction func about() {
        performSegue(withIdentifier: "about", sender: AnyObject.self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let red = CGFloat(drand48())
        let green = CGFloat(drand48())
        let blue = CGFloat(drand48())
        message.backgroundColor = UIColor(
            red:red,
            green:green,
            blue:blue,
            alpha:1.0
        )
        
    }
    
}
